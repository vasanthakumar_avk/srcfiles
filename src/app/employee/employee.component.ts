import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { EmployeeService } from '../shared/employee.service';
import { User } from '../shared/employee.model';
import * as CryptoJS from 'crypto-js';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css'],
  providers: [EmployeeService]
})

export class EmployeeComponent implements OnInit {

  constructor(public employeeService: EmployeeService, private toastr: ToastrService) { } 
   gender: string;

  ngOnInit() {
    this.resetForm();
    this.refreshEmployeeList();  
  }

  resetForm(form?: NgForm) {

    if (form)
      form.reset();
    this.employeeService.selectedEmployee = {
      _id: "",
      name: "",
      age: "",
      email: "",
      password: "",
      gender: "",
      country: "",
      agree: "",
      created_at: "",
      updated_at: "",
      userinfo: "",
      user_id: "",
    }
  }
  
  onSubmit(form: NgForm) {

    if(form.value.user_id == '' || form.value.user_id == null){
      this.employeeService.postEmployee(form.value).subscribe((res) => { 
          this.resetForm(form);
          this.refreshEmployeeList();
          this.toastr.success('New User has created!', 'Success!');
      })
    }else{
      this.employeeService.putEmployee(form.value).subscribe((res) => { 
        this.resetForm(form);
        this.refreshEmployeeList();
        this.toastr.success('updated successfuly', 'Success!');
      })
    };
  }

  refreshEmployeeList() {
    this.employeeService.getEmployeeList().subscribe((res) => {
      this.employeeService.employees = res as User[];
    });
  }

  onEdit(emp: User, form: NgForm) {
     if (confirm('Are you sure to edit this '+ emp.name +' record ?') == true) {
      this.employeeService.selectedEmployee = emp;
      this.toastr.success('selected successfuly', 'Success!');
    }
  }

  onDelete(user_id: string, form: NgForm) {
    if (confirm('Are you sure to delete this recorfd ?') == true) {

      this.employeeService.deleteEmployee(user_id).subscribe((res) => {
        this.refreshEmployeeList();
        this.resetForm(form);
        this.toastr.error('Deleted successfuly', 'Delete!');
      });
    }
  }
}