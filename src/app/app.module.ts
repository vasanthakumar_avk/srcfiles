import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs';

import { MonacoEditorModule, MONACO_PATH } from '@materia-ui/ngx-monaco-editor';
import { FlexLayoutModule } from '@angular/flex-layout';

import {FileUploadModule} from 'ng2-file-upload';
import { NgxJsonViewerModule } from 'ngx-json-viewer';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployeeComponent } from './employee/employee.component';
import { MatRadioModule } from '@angular/material/radio';
import { EmployeePipe } from './shared/employee.pipe';
import { EmployeemalePipe } from './shared/employeemale.pipe';
import { SigninComponent } from './signin/signin.component';
import { ProfileComponent } from './profile/profile.component';
import { BalanceComponent } from './profile/balance/balance.component';
import { HistoryComponent } from './profile/history/history.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { QrCodeModule } from 'ng-qrcode';
import { AuthenticationComponent } from './profile/authentication/authentication.component';
import { NavComponent } from './profile/nav/nav.component';
import { AuthdisableComponent } from './profile/authdisable/authdisable.component';
import { SecureComponent } from './profile/secure/secure.component';
import { TexteditorComponent } from './profile/texteditor/texteditor.component';
import { FileuploadComponent } from './profile/fileupload/fileupload.component';
import { JsonreaderComponent } from './profile/jsonreader/jsonreader.component';
import { TableDOMComponent } from './profile/table-dom/table-dom.component';
import { SafeHtmlPipe } from './profile/safe-html.pipe';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent,
    EmployeePipe,
    EmployeemalePipe,
    SigninComponent,
    ProfileComponent,
    BalanceComponent,
    HistoryComponent,
    AuthenticationComponent,
    NavComponent,
    AuthdisableComponent,
    SecureComponent,
    TexteditorComponent,
    FileuploadComponent,
    JsonreaderComponent,
    TableDOMComponent,
    SafeHtmlPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,ReactiveFormsModule,
    HttpClientModule,MatRadioModule, 
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    QrCodeModule,
    MonacoEditorModule,
    FlexLayoutModule,
    FileUploadModule,
    NgxJsonViewerModule,
  ],
  providers: [
    {
      provide: MONACO_PATH,
      useValue: 'https://unpkg.com/monaco-editor@0.31.1/min/vs',
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
