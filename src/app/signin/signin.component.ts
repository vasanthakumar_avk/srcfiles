import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { EmployeeService } from '../shared/employee.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
}) 
export class SigninComponent implements OnInit {

    model ={
    email :"",
    password:""
  };

  constructor(private router: Router, public employeeService: EmployeeService, private toastr: ToastrService) { }

  ngOnInit(): void {
  }
  
  onSubmit(form: NgForm) { 
      this.employeeService.authlogin(form.value).subscribe((data: any) => {
        if(data.token){
          let token = data.token;
          localStorage.setItem('Token', token);
          var act = data.status;

          if(act == "active"){
            this.router.navigate(['/secure']);
          }
          else{
            this.router.navigate(['/profilepage']);
            this.toastr.success('login!', 'Success!');
          };
        }
        else{
          this.toastr.error('invalid Login Details!', 'Waring!');
        }
      });
      form.reset();
    }
}
