import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeComponent } from './employee/employee.component';
import { SigninComponent } from './signin/signin.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuard } from './guard/auth.guard';
import { BalanceComponent } from './profile/balance/balance.component';
import { HistoryComponent } from './profile/history/history.component';
import { AuthenticationComponent } from './profile/authentication/authentication.component';
import { AuthdisableComponent } from './profile/authdisable/authdisable.component';
import { SecureComponent } from './profile/secure/secure.component';
import { TexteditorComponent } from './profile/texteditor/texteditor.component';
import { FileuploadComponent } from './profile/fileupload/fileupload.component';
import { JsonreaderComponent } from './profile/jsonreader/jsonreader.component';
import { TableDOMComponent } from './profile/table-dom/table-dom.component';

const routes: Routes = [
  {
    path: 'userregistration',
    component: EmployeeComponent
  },
  {
    path: 'usersignin',
    component: SigninComponent
  },
  {
    path: 'profilepage',
    canActivate: [ AuthGuard ],
    component: ProfileComponent
  },
  {
    path: 'balancepage',
    canActivate: [ AuthGuard ],
    component: BalanceComponent
  },
  {
    path: 'historypage',
    canActivate: [ AuthGuard ],
    component: HistoryComponent
  },
  {
    path: 'authentication',
    canActivate: [ AuthGuard ],
    component: AuthenticationComponent
  },
  {
    path: 'disableauth',
    canActivate: [ AuthGuard ],
    component: AuthdisableComponent
  },
  {
    path: 'secure',
    canActivate: [ AuthGuard ],
    component: SecureComponent
  },
  {
    path: 'texteditor',
    canActivate: [ AuthGuard ],
    component: TexteditorComponent
  },
  {
    path: 'fileupload',
    canActivate: [ AuthGuard ],
    component: FileuploadComponent
  },
  {
    path: 'jsonreader',
    canActivate: [ AuthGuard ],
    component: JsonreaderComponent
  },
  {
    path: 'table-dom',
    canActivate: [ AuthGuard ],
    component: TableDOMComponent
  },
  { path: '', redirectTo: '/userregistration', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
