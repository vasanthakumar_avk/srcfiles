import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../../shared/employee.service';
import { NgForm } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { ProfileComponent } from '../profile.component';

@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.css']
})
export class BalanceComponent implements OnInit {
  CoinBalance: any = [];
  selectedDevice: any = {
    tokenvalue: "",
  };
  BTC: string;
  ETH: string;
  XRP: string;

  constructor(private router: Router, public employeeService: EmployeeService) {}
  
  CoinBalanceListvalue(){
    var ngng = localStorage.getItem('Token');
      this.selectedDevice.tokenvalue = ngng;
    var balancetokenvalue = this.selectedDevice;
    this.employeeService.CoinBalanceList(balancetokenvalue).subscribe((res:any) => {
      this.CoinBalance = res;
    })
  };

  ngOnInit(): void {
    this.CoinBalanceListvalue();
  }

}
