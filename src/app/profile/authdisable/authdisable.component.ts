import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../../shared/employee.service';
import { NgForm } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-authdisable',
  templateUrl: './authdisable.component.html',
  styleUrls: ['./authdisable.component.css']
})
export class AuthdisableComponent implements OnInit {

  constructor(private router: Router, public employeeService: EmployeeService, private toastr: ToastrService) { }
  AuthentiValue: any = [];
  Checkauth: any = [];
  selectedDevice: any = {
    tokenvalue: "",
    token: "",
  };

  Authentication(){
    var ngng = localStorage.getItem('Token');
      this.selectedDevice.tokenvalue = ngng;
    var tokengen = this.selectedDevice;
    this.employeeService.twowayauthentication(tokengen).subscribe((res:any) => {
      this.AuthentiValue = res;
      this.selectedDevice.token = res.secretCode;
    })
  }

  AuthDisable(form: NgForm){
    this.employeeService.authdisable(this.selectedDevice).subscribe((res:any) => {
      this.Checkauth = res;
      if(this.Checkauth == true){
        this.toastr.info('two factor authentication has disabled', 'Success!');
        this.router.navigate(['/authentication']);
      }
      else{
        this.toastr.error('Wrong secret code...!', 'Wrong!');
      }
      form.reset();
    });
  }

  ngOnInit(): void {
    this.Authentication();
  }

}