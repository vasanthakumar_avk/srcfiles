import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthdisableComponent } from './authdisable.component';

describe('AuthdisableComponent', () => {
  let component: AuthdisableComponent;
  let fixture: ComponentFixture<AuthdisableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuthdisableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthdisableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
