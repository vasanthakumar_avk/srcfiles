import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../../shared/employee.service';
import { NgForm } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit {

  constructor(private router: Router, public employeeService: EmployeeService, private toastr: ToastrService) { }
  AuthentiValue: any = [];
  Checkauth: any = [];
  selectedDevice: any = {
    tokenvalue: "",
    token: "",
    secret: "",
  };

  Authentication(){
    var ngng = localStorage.getItem('Token');
      this.selectedDevice.tokenvalue = ngng;
    var tokengen = this.selectedDevice;
    this.employeeService.twowayauthentication(tokengen).subscribe((res:any) => {
      this.AuthentiValue = res;
      this.selectedDevice.token = res.secretCode;
    })
  }

  onSubmitAuth(form: NgForm){
    this.employeeService.authsecret(this.selectedDevice).subscribe((res:any) => {
      this.Checkauth = res;
      if(this.Checkauth == true){
        this.toastr.success('two factor authentication active now', 'Success!');
        this.router.navigate(['/disableauth']);
      }
      else{
        this.toastr.error('Wrong secret code...!', 'Warning!');
      }
    });
    form.reset();
  }

  ngOnInit(): void {
    this.Authentication();
  }

}
