import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableDOMComponent } from './table-dom.component';

describe('TableDOMComponent', () => {
  let component: TableDOMComponent;
  let fixture: ComponentFixture<TableDOMComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableDOMComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableDOMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
