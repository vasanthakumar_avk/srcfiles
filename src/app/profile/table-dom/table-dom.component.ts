import { Component, OnInit, ElementRef} from '@angular/core';

@Component({
  selector: 'app-table-dom',
  templateUrl: './table-dom.component.html',
  styleUrls: ['./table-dom.component.css']
})

export class TableDOMComponent implements OnInit {

  myTemplate  = '';

  constructor(private elementRef:ElementRef) { }

  openAlert() {
    alert('hello');
  }
  ngAfterViewChecked (){
    if(this.elementRef.nativeElement.querySelector('#my-button')){
      this.elementRef.nativeElement.querySelector('#my-button').addEventListener('click', this.openAlert);
    }
  } 
  ngOnInit(): void {
    this.myTemplate  = '<h1>HEllO</h1><p>testone and testtwo</p><button type="button" id="my-button" (click)="openAlert()">click mee</buttn>';
  }

}

// .bind(this)
