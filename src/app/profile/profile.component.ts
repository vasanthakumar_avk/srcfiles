import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../shared/employee.service';
import { NgForm } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  Coin: any = [];
  CoinValue: any = [];
  CoinPrice: any = [];
  CoinBalance: any = [];
  CoinHistroy: any = [];
  BTC: any;
  ETH: any;
  XRP: any;
  selectedDevice: any = {
    first_currency: "",
    second_currency: "",
    market_price: "",
    transaction_fee: "",
    amount: "",
    totalprice: "",
    ERKBalance: "",
    IPXBalance: "",
    SOTBalance: "",
    tokenvalue: "",
  };

  constructor(private router: Router, public employeeService: EmployeeService, private toastr: ToastrService) { }

  refreshCoinList() {
    this.employeeService.coinList().subscribe((res:any) => {
      this.Coin = res;
    });
  }

  onSelect(first_currency : any){
      this.selectedDevice.first_currency = first_currency.value;
      this.employeeService.coinListValue(this.selectedDevice).subscribe((res:any) => {
        this.CoinValue = res;
      })
    };

  onMarketprice(second_currency : any){
    this.selectedDevice.second_currency = second_currency.value;
    this.employeeService.coinmarket_price(this.selectedDevice).subscribe((res: any) => {
      this.CoinPrice = res;
      this.selectedDevice.transaction_fee = this.CoinPrice.transaction_fee;
      this.selectedDevice.market_price = this.CoinPrice.market_price;
    })
  }

  search(amount: any){
    var ngng = localStorage.getItem('Token');
      this.selectedDevice.tokenvalue = ngng;

    this.selectedDevice.amount = amount.value;
    var balancetokenvalue = this.selectedDevice;
    this.employeeService.CoinBalanceList(balancetokenvalue).subscribe((res:any) => {
      this.selectedDevice.ERKBalance = res.ERK;
      this.selectedDevice.IPXBalance = res.IPX;
      this.selectedDevice.SOTBalance = res.SOT;


      var amountbalance = Number(this.selectedDevice.amount);

      if(amountbalance > res[this.selectedDevice.first_currency]){
        this.toastr.warning("Your "+ this.selectedDevice.first_currency +" Balance is " + res[this.selectedDevice.first_currency] +" so it's not enough to exchange! ", 'Waring!');
        console.log("Your "+ this.selectedDevice.first_currency +" Balance is " + res[this.selectedDevice.first_currency] +" so it's not enough to exchange ");
      }    
    });
  }

  CoinBalanceListvalue(){
    var ngng = localStorage.getItem('Token');
      this.selectedDevice.tokenvalue = ngng;

    var balancetokenvalue = this.selectedDevice;
    this.employeeService.CoinBalanceList(balancetokenvalue).subscribe((res:any) => {
      this.CoinBalance = res;
    })
  };

  onSubmitAmount(form: NgForm){
    if(Number(this.selectedDevice.amount) > this.CoinBalance[this.selectedDevice.first_currency] || Number(this.selectedDevice.amount) == 0){
      this.toastr.warning("Your " + this.selectedDevice.first_currency +" Balance is " + this.CoinBalance[this.selectedDevice.first_currency] + " " +" so it's not enough to exchange!", 'Waring!');
    }
    else {
      this.employeeService.CoinBalanceListChange(this.selectedDevice).subscribe((res:any) => {
        this.CoinBalanceListvalue();
        this.showSuccess();
      })
      form.reset();
    }
  }

  ngOnInit() {
    this.refreshCoinList();
    this.CoinBalanceListvalue();
    this.onSelect(this.selectedDevice);
    // this.selectedDevice.first_currency = "ETH";
  }

  showSuccess() {
    this.toastr.success('Exchanged!', 'Success!');
  }
}
