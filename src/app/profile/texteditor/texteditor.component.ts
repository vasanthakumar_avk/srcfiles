import { Component, ViewChild, OnInit } from '@angular/core';
import {
  MonacoEditorComponent,
  MonacoEditorConstructionOptions,
  MonacoEditorLoaderService,
  MonacoStandaloneCodeEditor
} from '@materia-ui/ngx-monaco-editor';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-texteditor',
  templateUrl: './texteditor.component.html',
  styleUrls: ['./texteditor.component.css'],
})
export class TexteditorComponent implements OnInit {
  valuefile: any = [];
  file: any;

  @ViewChild(MonacoEditorComponent, { static: false })
    monacoComponent: MonacoEditorComponent;
    editorOptions: MonacoEditorConstructionOptions = {
      theme: 'vs-dark',
      language: 'javascript',
      roundedSelection: true,
      autoIndent: 'full'
    };

  constructor(private monacoLoaderService: MonacoEditorLoaderService, private toastr: ToastrService) {
  }

  editorInit(editor: MonacoStandaloneCodeEditor) {
    editor.setSelection({
      startLineNumber: 1,
      startColumn: 1,
      endColumn: 50,
      endLineNumber: 3
    });
  }

  SelectFile(event:any){
    this.file = event.target.files[0];
    var valid = this.file.name.split('.').pop();

    if(valid == "sol"){
      let fileReader = new FileReader();
        fileReader.onload = (e) => {
          var Selectedfile = fileReader.result;
          this.valuefile = Selectedfile;
        }
      fileReader.readAsText(this.file);
      this.toastr.success('Selected sol file type', 'Success!');
    }
    else{
      this.file = event.target.value = null;
      this.toastr.error('Select sol file type', 'Wrong!');
    }

  }

  ngOnInit(): void {
  }
}
