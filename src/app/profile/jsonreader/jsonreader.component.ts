import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-jsonreader',
  templateUrl: './jsonreader.component.html',
  styleUrls: ['./jsonreader.component.css']
})
export class JsonreaderComponent implements OnInit {
  data = {
    "First_Data" :{
      "name":"vasanthakumar",
      "email": "vasanthanbu24@gmail.com",
      "password": "vasanth123",      
    },
    "Second_Data" : {
      "gender": "male",
      "D.O.B":"13/06/1999",
      "Qulification": "B.E",
      "language": "Tamil, Engilsh"
    },
    "Address_Data" : {
      "Door_No" : "17-1-537/2",
      "street": "near HP petrole bulk, main road",
      "city": "vadipatti",
      "distic": "madurai",
      "pincode": "625218"
    }

  }
  constructor() { }

  ngOnInit(): void {
  }

}
