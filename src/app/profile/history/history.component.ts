import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../../shared/employee.service';
import { NgForm } from '@angular/forms';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  CoinHistroy: any = [];
  selectedDevice: any = {
    tokenvalue: "",
  };

  constructor(private router: Router, public employeeService: EmployeeService) {}
  

  History(){
    var ngng = localStorage.getItem('Token');
      this.selectedDevice.tokenvalue = ngng;
    var tokengen = this.selectedDevice;
    this.employeeService.HistoryList(tokengen).subscribe((value:any) => {
    this.CoinHistroy = value;
     
    });
  }

  ngOnInit(): void {
    this.History();
  }

}
