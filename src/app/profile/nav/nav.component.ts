import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../../shared/employee.service';
import { NgForm } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  entry : string;
  AuthentiValue: any = [];
  selectedDevice: any = {
    tokenvalue: "",
    token: "",
  };

  constructor(private router: Router, public employeeService: EmployeeService, private toastr: ToastrService) { }
 
  logout() {
    if (confirm('Are you sure to log out this profile ?') == true) {
      localStorage.removeItem('Token');
      this.router.navigate([ '/usersignin' ]);
      this.toastr.success('logout!', 'Success!');
    }
  }

  Authentication(){
    var ngng = localStorage.getItem('Token');
      this.selectedDevice.tokenvalue = ngng;
    var tokengen = this.selectedDevice;
    this.employeeService.twowayauthentication(tokengen).subscribe((res:any) => {
      this.AuthentiValue = res;
      this.selectedDevice.token = res.secretCode;
      if(this.AuthentiValue.status == "active"){
        this.entry = "/disableauth"; 
      }
      else{
        this.entry = "/authentication";
      }
    })
  }

  ngOnInit(): void {
    this.Authentication();
  }

}
