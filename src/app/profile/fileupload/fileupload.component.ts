import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { EmployeeService } from '../../shared/employee.service';

@Component({
  selector: 'app-fileupload',
  templateUrl: './fileupload.component.html',
  styleUrls: ['./fileupload.component.css']
})
export class FileuploadComponent implements OnInit {
  images :any;
  multipleImages = [];

  constructor(private http: HttpClient, public employeeService: EmployeeService) { }

  selectImage(event :any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.images = file;
    }
  }

  onSubmit(){
    const formData = new FormData();
    formData.append('file', this.images);

    this.employeeService.filemanydata(formData).subscribe((res:any) => {
    })
  }

  // selectMultipleImage(event :any){
  //   if (event.target.files.length > 0) {
  //     this.multipleImages = event.target.files;
  //   }
  // }

  // onMultipleSubmit(){
  //   const formData = new FormData();
  //   for(let img of this.multipleImages){
  //     formData.append('files', img);
  //   }
  //   this.employeeService.fileonedata(formData).subscribe((res:any) => {
  //     console.log(res);
  //   })
  // }

  ngOnInit(): void {
  }
}