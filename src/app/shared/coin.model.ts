export class Coin {
	_id: String;
	first_currency: String;
	second_currency: String;
	market_price: String;
	transaction_fee: String;
	min_amt: String;
}