import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import 'rxjs-compat/add/operator/map';
import 'rxjs-compat/add/operator/toPromise';
import * as cryptoJs from 'crypto-js';
import { environment } from '../../environments/environment';

import { User } from './employee.model'; 

@Injectable({
   providedIn: 'root'
})

export class EmployeeService {
  selectedEmployee: User;
  employees: User[];
  
  //readonly baseURL = 'http://localhost:3000/user';

  constructor(private http : HttpClient) { }

  postEmployee(emp : User) {
    return this.http.post(`${environment.baseURL}user`, emp);
  }       

  authlogin(emp : User) {
    return this.http.post(`${environment.baseURL}auth`, emp);
  }

  getEmployeeList() {
    return this.http.get(`${environment.baseURL}user`);
  }

  coinList() {
    return this.http.get(`${environment.baseURL}coin`);
  }

  CoinBalanceList(balancetokenvalue: any) {
    return this.http.get(`${environment.baseURL}balance`, { params: balancetokenvalue});
  }

  HistoryList(tokengen: any){
    return this.http.get(`${environment.baseURL}histroy`,{ params: tokengen});
  }

  CoinBalanceListChange(amount: any,) {
    return this.http.post(`${environment.baseURL}balance`, amount);
  }

  coinListValue(first_currency : any) {
    return this.http.post(`${environment.baseURL}coin`, first_currency);
  }

  coinmarket_price(second_currency : any) {
    return this.http.post(`${environment.baseURL}coinmarketvalue`, second_currency);
  }

  putEmployee(emp: User) {
    return this.http.put(`${environment.baseURL}user` + `/${emp.user_id}`, emp);
  }

  deleteEmployee(user_id: string) {
    return this.http.delete(`${environment.baseURL}user`+ `/${user_id}`);
  }
  twowayauthentication(tokengen: any){
    return this.http.get(`${environment.baseURL}authentication`,{ params: tokengen});
  }
  authsecret(secret: any){
    return this.http.post(`${environment.baseURL}authentication`, secret);
  }
  authdisable(secret: any){
    return this.http.post(`${environment.baseURL}authentidisable`, secret);
  }
  filemanydata(formData: any){
    return this.http.post(`${environment.baseURL}file`,formData)
  }
  // fileonedata(formData: any){
  //   return this.http.post(`${environment.baseURL}multipleFiles`, formData);
  // }
}