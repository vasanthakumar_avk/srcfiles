export class User {
	_id: string;
	name: string;
	age: string;
	email: string;
	password: string;
	gender: string;
	country: string;
	agree: string;
	created_at: string;
	updated_at: string;
	userinfo: string;
	user_id: string;
}
