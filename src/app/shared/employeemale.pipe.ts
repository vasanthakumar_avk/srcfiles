import { Pipe, PipeTransform, Injectable } from '@angular/core';
import { EmployeeService } from '../shared/employee.service';

@Pipe({
  name: 'filtermale'
})
@Injectable()
export class EmployeemalePipe implements PipeTransform {

  transform(items: any[], field : string, value: string): any[] {
    if (!items) return [];
    if (!value || value.length === 0) return items;
    return items.filter(it =>
    it[field] === value);
  }
}