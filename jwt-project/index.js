const http = require("http");
const app = require("./app");
const server = http.createServer(app);

const { API_PORT } = process.env;
const port = process.env.PORT || API_PORT;

server.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
/*
http://localhost:4001/login
http://localhost:4001/register
http://localhost:4001/welcome

access tokem(jwt)
x-access-token=
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjFlZTg5MmUyYmE2ZGVjOTZlY2U5OTU5IiwiZW1haWwiOiJkdWRlQGd5amFzLnBsbSIsImlhdCI6MTY0MzAyMjYzOCwiZXhwIjoxNjQzMDI5ODM4fQ.X089m4q_QsYXLbfDC_0LOMYxUsDXnC9PvRIYMJXIpcU
*/