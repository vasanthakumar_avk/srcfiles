const express = require('express');
var router = express.Router();
const assert = require('assert');
var { User } = require('../models/user');
var { UserInfo } = require('../models/userinfo');
var { CoinDetails } = require('../models/coin');
var { HistoryDetails } = require('../models/history');
var { CoinBalance } = require('../models/coinbalance');
var { Authentication } = require('../models/authentication');
var ObjectId = require('mongoose').Types.ObjectId;
var encrypted = require('./encrypt.js');
const auth = require("../middleware/auth")
const jwt = require("jsonwebtoken");
const speakeasy = require('speakeasy');
const QRCode = require('qrcode');
const axios = require('axios');
const multer = require('multer');
// => http://localhost:3000/user/
	
	const storage = multer.diskStorage({
	    destination: (req, file, callBack) => {
	        callBack(null, 'uploads')
	    },
	    filename: (req, file, callBack) => {
	        callBack(null, `NewFileUploaded__${file.originalname}`)
	    }
	  })
	  
	const upload = multer({ storage: storage })

	  router.post('/file', upload.single('file'), (req, res, next) => {
	    const file = req.file;
	    if (!file) {
	      const error = new Error('No File')
	      error.httpStatusCode = 400
	      return next(error)
	    }
	      res.send(file);
	  })

	  // router.post('/multipleFiles', upload.array('files'), (req, res, next) => {
	  //   const files = req.files;
	  //   console.log(files);
	  //   if (!files) {
	  //     const error = new Error('No File')
	  //     error.httpStatusCode = 400
	  //     return next(error)
	  //   }
	  //     res.send({sttus:  'ok'});
	  // })

	router.get('/coin' , async(req, res) => {
  		CoinDetails.find().distinct('first_currency',function(err, results){
			res.send(results)
		});
	});

	router.get('/balance', async(req, res) => {
		const balancetoken = {
			token: req.query.tokenvalue,
		}
		var tokenval = balancetoken.token;
		var decoded = jwt.decode(tokenval);
		CoinBalance.findOne({user_coin: ObjectId(decoded.user._id)},(err, doc) =>{
			res.send(doc);
		});
	})

	router.post('/balance', async(req, res) => {
		const balancevavlue = {
			first_currency: req.body.first_currency,
			second_currency: req.body.second_currency,
			amount: req.body.amount,
			token: req.body.tokenvalue,
		}
		try {
			let marketvalue = await CoinDetails.findOne({
				first_currency: balancevavlue.first_currency,
				second_currency: balancevavlue.second_currency
			});

			if(marketvalue) {
				let response = null;
				new Promise(async (resolve, reject) => {
				  try {
				    response = await axios.get('https://min-api.cryptocompare.com/data/pricemultifull?fsyms='+[marketvalue.first_currency]+'&tsyms='+[marketvalue.second_currency]+'', {
				      headers: {
				        'X-CMC_PRO_API_KEY': 'b8edd1ce91bf9ab7ec3988ee32ed03e27c36821bd6aadc2a5fc62ceddb8f7e6a',
				      },
				    });
				  } catch(ex) {
				    response = null;
				    console.log(ex);
				    reject(ex);
				  }
				  if (response) {
				    const json = response.data;

					var second_currency = marketvalue.second_currency;
					var first_currency = marketvalue.first_currency;
					var feevalue = Number(marketvalue.transaction_fee)
				    var marketprice = json.RAW[first_currency][second_currency].PRICE;

					var tokenval = balancevavlue.token;
					var decoded = jwt.decode(tokenval);
					CoinBalance.findOne({user_coin: ObjectId(decoded.user._id)},(err, doc)=> {
						var valueSub = Number(doc[first_currency]);
						var valueAdd = Number(doc[second_currency]);
						var markvalue = Number(marketprice);

						const AddAccount = {
							[second_currency]: (valueAdd + ((Number(balancevavlue.amount) * markvalue)-((Number(balancevavlue.amount) * markvalue)*(feevalue/100)))),
						}

						const subAccount = {
							[first_currency]: (valueSub - balancevavlue.amount)
						}
						
						const Totaltransaction_fee = {
							[first_currency] : ((Number(balancevavlue.amount)*markvalue)*(feevalue/100)),
						}

						const TotalRecivedAmount = {
							[first_currency] : (((Number(balancevavlue.amount)*markvalue))-((Number(balancevavlue.amount) * markvalue)*(feevalue/100))),
						}

						CoinBalance.updateOne({user_coin: {$in: decoded.user._id}},{ $set: subAccount }, (err, doc) => {
							if(!err) {
								CoinBalance.updateOne({user_coin: {$in: decoded.user._id}},{ $set: AddAccount }, (err, docoas) => {
									if(!err) {
										var add_currecncy = AddAccount[second_currency];
										var sub_currecncy = subAccount[first_currency];
										var totaltransaction_fee = Totaltransaction_fee[first_currency];
										var totalrecivedamount = TotalRecivedAmount[first_currency];
										var HistoryValue = new HistoryDetails({
											first_currency: req.body.first_currency,
											second_currency: req.body.second_currency,
											ExchangeAmount: req.body.amount,
											market_price: marketprice,
											transaction_fee: marketvalue.transaction_fee,
											ExchangedAccountBalance: sub_currecncy,
											transactionAccountBalance: add_currecncy,
											TotaltransactionFee: totaltransaction_fee,
											TotalRecivedValue: totalrecivedamount,
											user_histroy : decoded.user._id,
											created_at: new Date(),
											updated_at: new Date(),
										});
										HistoryValue.save().then(function(data){
											res.send(data);
										})
									}
								});
							}
						});
					})
				  }
				})

			}
			
		}
		catch (err) {
			console.log(err)
			res.status(500).json({
				type: "Something Went Wrong",
				msg : err
			})
		}
	});

	router.get('/histroy', async(req, res) => {
		const historytoken = {
			token: req.query.tokenvalue,
		}
		var tokenval = historytoken.token;
		var decoded = jwt.decode(tokenval);

		HistoryDetails.find({user_histroy: ObjectId(decoded.user._id)},(err, doc) => {
		res.send(doc);
		});
	});

	router.get('/authentication', async(req, res)=> {
		const authetoken = {
			token: req.query.tokenvalue,
		}
		var tokenval = authetoken.token;
		var decorded = jwt.decode(tokenval);

		Authentication.findOne({authenticationverify: ObjectId(decorded.user._id)}, (err, doc)=> {
			res.send(doc);
		});
	});

	router.post('/authentication', async(req, res)=> {

		var verified = speakeasy.totp.verify({
			secret: req.body.token,
			encoding: 'ascii',
			token: req.body.secret,
		})

		var authetoken = {
			token: req.body.tokenvalue,
		}
		var tokenval = authetoken.token;
		var decoded = jwt.decode(tokenval);

		if(verified){
			Authentication.updateOne({authenticationverify: {$in: decoded.user._id}},{ $set: {status: 'active'} }, { multi: true }, (err, doc) => {
			});
		}else {
			console.log("Wrong secretCode!");
		}
		res.send(verified);
	});

	router.post('/authentidisable', async(req, res)=> { 

		var verified = speakeasy.totp.verify({
			secret: req.body.token,
			encoding: 'ascii',
			token: req.body.secret,
		})

		var authetoken = {
			token: req.body.tokenvalue,
		}
		var tokenval = authetoken.token;
		var decoded = jwt.decode(tokenval);

		if(verified){
			var secretotp = speakeasy.generateSecret({
				name: decoded.user.email,
			});
			QRCode.toDataURL(secretotp.otpauth_url, function(err, data) {
				var secretvalue = {
					QRCode: data,
					secretCode : secretotp.ascii,
					status: 'inactive'
				}
				Authentication.updateOne({authenticationverify: {$in: decoded.user._id}},{ $set: secretvalue }, { multi: true }, (err, doc) => {
				});
			});
		}else {
			console.log("Wrong secretCode!");
		}
		res.send(verified);
	})

	router.post('/coin', async(req, res) => {
		const coinValue = {
			first_currency : req.body.first_currency,
			second_currency : req.body.second_currency,
			amount: req.body.amount,
		}
		try {
			let coinvalues = await CoinDetails.find({
				first_currency: coinValue.first_currency,
			});
			if(coinvalues) {
				res.send(coinvalues);
			}
		}
		catch (err) {
	        console.log(err)
	        res.status(500).json({
	            type: "Something Went Wrong",
	            msg: err
	        })
	    }
	});

	router.post('/coinmarketvalue', async(req, res) => {
		const coinMarketValue = {
			first_currency: req.body.first_currency,
			second_currency: req.body.second_currency,
		}
		try {
			let marketvalue = await CoinDetails.findOne({
				first_currency: coinMarketValue.first_currency,
				second_currency: coinMarketValue.second_currency
			});
			if(marketvalue) {
				let response = null;
				new Promise(async (resolve, reject) => {
				  try {
				    response = await axios.get('https://min-api.cryptocompare.com/data/pricemultifull?fsyms='+[marketvalue.first_currency]+'&tsyms='+[marketvalue.second_currency]+'', {
				      headers: {
				        'X-CMC_PRO_API_KEY': 'b8edd1ce91bf9ab7ec3988ee32ed03e27c36821bd6aadc2a5fc62ceddb8f7e6a',
				      },
				    });
				  } catch(ex) {
				    response = null;
				    console.log(ex);
				    reject(ex);
				  }
				  if (response) {
				    const json = response.data;
					var feevalue = Number(marketvalue.transaction_fee);
				    var marketprice = json.RAW[marketvalue.first_currency][marketvalue.second_currency].PRICE;
				    value = {
				    	market_price : marketprice,
				    	transaction_fee: feevalue,
				    }
					res.send(value);
				}
			});
			}
		}
		catch (err) {
			console.log(err)
			res.status(500).json({
				type: "Something Went Wrong",
				msg : err
			})
		}
	});
	
	router.get("/user", async (req, res) => {

		User.aggregate([
		  {
		    $lookup: {
			  from: 'userinfos',
			  localField: '_id',
			  foreignField: 'user_profile',
			  as: 'userinfo'
			}
		  },
		  {
		    $unwind: "$userinfo",
		  },
		  {
		  	$project: {
		  		_id: 0,
		  		user_id:`$_id`,		
		  		name: 1,
		  		email: 1,
		  		password: 1,
		  		created_at : 1,
		  		updated_at:1,
	  			age 	:`$userinfo.age`,
	  			gender 	:`$userinfo.gender`,
	  			country :`$userinfo.country`,
		     }
		   },
		]).sort({age: 1}).collation({
	         locale: "en_US",
	         numericOrdering: true
	      }).then((User) => {
				User.forEach((User) => {
					var data = User.user_id.toString(); 
					User.user_id = encrypted.encrypt(data);

					let value = User.user_id;						//**
					const checkone = encrypted.decrypt(value)
				})
				res.send(User);
		   });
	});

	router.post('/user', (req, res) => {
		var secret = speakeasy.generateSecret({
			name: req.body.email,
		});

		QRCode.toDataURL(secret.otpauth_url, function(err, data) {

			var empen = new UserInfo({
				age: req.body.age,
				gender: req.body.gender,
				country: req.body.country,
				agree: req.body.agree,
				user_profile: req.body._id,
			});

			var emp = new User({
				name: req.body.name,
				email: req.body.email,
				password: req.body.password,
				created_at: new Date(),
				updated_at: new Date(),
			});

			var fresh = new CoinBalance ({
				BTC: "0",
				ETH: "0",
				XRP: "0",
				user_coin: req.body._id,
			});
			var authen = new Authentication ({
				QRCode: data,
				secretCode : secret.ascii,
				authenticationverify: req.body._id,
			})

			empen.user_profile = emp._id;
			fresh.user_coin = emp._id;
			authen.authenticationverify = emp._id;

			var pass = emp.password.toString(); 
			emp.password = encrypted.encrypt(pass);

			emp.save().then(empen.save()).then(fresh.save()).then(authen.save()).then(function(data){
		    	res.send(data);
			}).catch(function (err) {
			    res.status(400).send('Uh oh! Something bad happened.');  
			    console.error(err);
			   });
		});
	});

	router.post('/auth', async(req, res) => {
	    const login = {
	        email: req.body.email,
	        password: req.body.password
	    }
	    try {
	        let user = await User.findOne({
	            email: login.email
	        });

	        var pass = login.password.toString(); 
			login.password = encrypted.encrypt(pass);

	        let match = login.password == user.password; 
     
	        if (match) {
	            let token = jwt.sign({user}, "secret", {expiresIn: 604800})

	            var tokenval = token;
				var decoded = jwt.decode(tokenval);

	            let value = Authentication.findOne({authenticationverify:ObjectId(decoded.user._id)}, (err, doc)=> {
		            if (token) {
		                res.status(200).json({
		                    success: true,
		                    token: token,
		                    status: doc.status,
		                    userCredentials: user
		                })
		            }
	            });
	        } 
	        else {
	        	res.status(200).json({
		    		status: false,
		    		msg: "Wrong Login Details"
	    		})
	        }
	    } catch (err) {
	    	res.status(200).json({
	    		status: false,
	    		msg: "Wrong Login Details"
	    	})
	      }
	});

	router.get('/data', auth, async(req, res) => {
		res.json({
        message: "your authorization Compelet now"
    	})
	})

	router.put('/user/:id', async(req, res) => {		
		let value = req.params.id; //**
		const checkone = encrypted.decrypt(value)
	  	console.log("putdecrypt" +" "+ checkone);

		if (!ObjectId.isValid(checkone))
			return res.status(400).send(`No record with given id : ${checkone}`);

		var empen = {
			age: req.body.age,
			gender: req.body.gender,
			country: req.body.country,
			user_profile: req.body._id,
		}
		
		var emp = {
			name: req.body.name,
			email: req.body.email,
			password: req.body.password, 
			updated_at: new Date(),
		};

		empen.user_profile = emp._id;

		User.updateMany({_id: {$in: checkone}},{ $set: emp }, (err, doc) => {
			if(!err) { res.send(doc); }
			else{ console.log('Error in User Update :' + JSON.stringify(err, undefined, 2)); }
		})

		UserInfo.updateMany({user_profile: {$in: checkone}},{ $set: empen }, (err, doc) => {
			if(err) { console.log('Error in User Update :' + JSON.stringify(err, undefined, 2)); }
		});
	});

	router.delete('/user/:id', (req, res) => { 
		let value = req.params.id;
		const checkone = encrypted.decrypt(value)
	  	console.log("deletedecrpt"+ " " + checkone);

		if(!ObjectId.isValid(checkone))
			return res.status(400).send(`No record with given id : ${checkone}`);

		User.deleteMany({_id: {$in: checkone}}, (err, doc) => {
			if(err) { console.log('Errpr in User Delete :' + JSON.stringify(err, undefined, 2)); }
		});

		UserInfo.deleteMany({user_profile: {$in: checkone}}, (err, doc) => {
			if(!err) { res.send(doc); }
			else{ console.log('Errpr in UserInfo Delete :' + JSON.stringify(err, undefined, 2)); }
		});
	});

module.exports = router;
