const express = require('express');

var crypto = require('crypto');
var assert = require('assert');


exports.encrypt =  function (data) { 
const iv = crypto.randomBytes(16);
    var cipher = crypto.createCipher("aes256", "helloworld", iv);  
    var encrypted = cipher.update(data, 'utf8', 'hex');
    encrypted += cipher.final('hex');
    return encrypted;
};

exports.decrypt = function (value) { 
const iv = crypto.randomBytes(16);
    var decipher = crypto.createDecipher("aes256", "helloworld", iv);
    var decrypted = decipher.update(value, 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    return decrypted; 
};


//encrypted.decrypt(value);
//encrypted.encrypt(data);

/* 
base64, hex
    //crypto(encryption):-

    const crypto=require("crypto-js");
    var data = empen.userid;
    var key = "helloworld";
    var encrypted = crypto.AES.encrypt(data, key).toString();
    var decrypted = crypto.AES.decrypt(encrypted, key)
        .toString(crypto.enc.Utf8);

    empen.userid = encrypted;
*/
