const mongoose = require('mongoose');
var Schema = mongoose.Schema; 
var uniqueValidator = require('mongoose-unique-validator');

var { UserInfo } = require('./userinfo');

var userSchema = new Schema({
	name: { 
		type: String,
		unique: true,
		//set: v => v.toUpperCase(),
		validate: {
			validator: async function(name) {
				const useren = await User.findOne({ name }); 
				if(useren) {
					if(this.id === useren.id) {
						return true;
					}
					return false;
				}
				return true;
			},
			message: props => 'The specified name is already in use.' 
		},
		required: [true, 'User name required'],
	},
	email: { 
		type: String,
		unique: true,
		validate: {
			validator: async function(email) {
				const useren = await User.findOne({ email }); 
				if(useren) {
					if(this.id === useren.id) {
						return true;
					}
					return false;
				}
				return true;
			},
			message: props => 'The specified email is already in use.' 
		},
		required: [true, 'User email required'],
	},
	password: { type: String },
	created_at : { type : Date, default: Date.now },
	updated_at: { type : Date, default: Date.now },
	userinfo: {
		type: Schema.Types.ObjectId,
		ref:'UserInfo'
	},
},{ versionKey: false },{ collection: 'users'});

var User = mongoose.model('User', userSchema); 

module.exports =  {
	User: User,
};
