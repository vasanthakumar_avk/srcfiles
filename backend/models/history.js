const mongoose = require('mongoose');
var Schema = mongoose.Schema;  

var HistoryDetailsSchema = new Schema({
	first_currency: { 
		type: String 
	},
	second_currency: { 
		type: String 
	},
	ExchangeAmount: {
		type: String
	},
	market_price: { 
		type: String ,
	},
	transaction_fee: { 
		type: String 
	},
	ExchangedAccountBalance: {
		type: String
	},
	transactionAccountBalance: {
		type: String
	},
	TotaltransactionFee: {
		type: String
	},
	TotalRecivedValue: {
		type: String
	},
	created_at : {
		 type : Date, default: Date.now 
	},
	updated_at: {
		 type : Date, default: Date.now 
	},
	user_histroy: {
		type: Schema.Types.ObjectId,
		ref:'User'
	},

},{ versionKey: false });

var HistoryDetails = mongoose.model('HistoryDetails', HistoryDetailsSchema);   

module.exports =  {
	HistoryDetails: HistoryDetails,
};
