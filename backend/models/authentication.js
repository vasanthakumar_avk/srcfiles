const mongoose = require('mongoose');
var Schema = mongoose.Schema;  

var AuthenticationSchema = new Schema({
	QRCode: { 
		type: String ,
	},
	secretCode: {
		type: String,
	},
	status: {
		Type : String,
	},
	authenticationverify: {
		type: Schema.Types.ObjectId,
		ref:'User'
	},
});

var Authentication = mongoose.model('Authentication', AuthenticationSchema);   

module.exports =  {
	Authentication: Authentication,
};