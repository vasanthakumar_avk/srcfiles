const mongoose = require('mongoose');
var Schema = mongoose.Schema;  

var { User } = require('./user');

var CoinDetailsSchema = new Schema({
	first_currency: { 
		type: String 
	},
	second_currency: { 
		type: String 
	},
	market_price: { 
		type: String ,
	},
	transaction_fee: { 
		type: String 
	},
	
	min_amt: {
		type: String 
	},  
});

var CoinDetails = mongoose.model('CoinDetails', CoinDetailsSchema);   

module.exports =  {
	CoinDetails: CoinDetails,
};
