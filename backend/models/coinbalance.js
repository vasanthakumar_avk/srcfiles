const mongoose = require('mongoose');
var Schema = mongoose.Schema;  

var CoinBalanceSchema = new Schema({
	BTC: { 
		type: String 
	},
	ETH: { 
		type: String 
	},
	XRP: { 
		type: String ,
	},
	user_coin: {
		type: Schema.Types.ObjectId,
		ref:'User'
	},
});

var CoinBalance = mongoose.model('CoinBalance', CoinBalanceSchema);   

module.exports =  {
	CoinBalance: CoinBalance,
};
