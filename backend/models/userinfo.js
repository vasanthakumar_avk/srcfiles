const mongoose = require('mongoose');
var Schema = mongoose.Schema;  

var { User } = require('./user');

var userDetailSchema = new Schema({
	age: { 
		type: String 
	},
	gender: { 
		type: String 
	},
	country: { 
		type: String ,
	},
	agree: { 
		type: String 
	},
	
	user_profile: {
		type: Schema.Types.ObjectId,
		ref:'User'
	},  
},{ versionKey: false },{ collection: 'userinfos'});

var UserInfo = mongoose.model('UserInfo', userDetailSchema);   

module.exports =  {
	UserInfo: UserInfo,
};
